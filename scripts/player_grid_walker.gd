extends TileMapWalker


func populate_moveset() -> void:
	var m_h := Input.get_axis("ui_left", "ui_right")
	var m_v := Input.get_axis("ui_up", "ui_down")
	push_move(Vector2(m_h, m_v))
