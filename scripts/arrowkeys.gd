extends Control


func _process(_delta: float) -> void:
	rect_position = get_tree().root.size - rect_size
	$up.visible = Input.is_action_pressed("ui_up")
	$down.visible = Input.is_action_pressed("ui_down")
	$right.visible = Input.is_action_pressed("ui_right")
	$left.visible = Input.is_action_pressed("ui_left")
